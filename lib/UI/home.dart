import 'package:stop_watch/UI/stop_watch.dart';

import '../UI/timer.dart';
import 'package:flutter/material.dart';
import 'package:stop_watch/stream/counter_stream.dart';

class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late Widget currentBody;
  late String title = "Clock";
  Widget stopwatch = const StopWatchScreen();
  Widget timer = const TimerScreen();

  //navigation bar var and func
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      switch (index) {
        case 0:
          title = "Stop Clock";
          currentBody = stopwatch;
          break;
        case 1:
          title = "Timer";
          currentBody = timer;
          break;
        case 2:
          title = "Alarm";
          break;
        default:
      }
    });
  }
  //navigation bar var and func

  //init state
  @override
  void initState() {
    super.initState();
    title = "Stop Watch";
    currentBody = const StopWatchScreen();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: currentBody,
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.watch_later),
            label: 'Stop Watch',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.timer),
            label: 'Timer',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.alarm),
            label: 'Alarm',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue,
        onTap: _onItemTapped,
      ),
    ));
    throw UnimplementedError();
  }
}
