import 'package:flutter/material.dart';
import 'dart:async';

class StopWatchScreen extends StatefulWidget {
  const StopWatchScreen({Key? key}) : super(key: key);

  @override
  State<StopWatchScreen> createState() => _StopWatchScreenState();
}

class _StopWatchScreenState extends State<StopWatchScreen> {
  List<Lap> laps = [];
  int seconds = 0;
  int lapPoint = 0;
  late Timer _timer;
  bool isCounting = false;

  ElevatedButton cancelButton() {
    return ElevatedButton(
        onPressed: () {
          setState(() {
            isCounting = false;
            _timer.cancel();
          });
        },
        style: ButtonStyle(
          minimumSize: MaterialStateProperty.all<Size>(const Size(50, 50)),
          backgroundColor: MaterialStateProperty.all<Color>(
              Color.fromARGB(255, 115, 113, 0)),
          shape: MaterialStateProperty.all<CircleBorder>(const CircleBorder()),
        ),
        child: const Text("Stop"));
  }

  ElevatedButton startButton() {
    return ElevatedButton(
      child: const Text("Start"),
      onPressed: () {
        // laps.add(Lap());
        // int newLapIndex = laps.length - 1;
        // currentLap = newLapIndex;
        // laps[newLapIndex]._timer =
        setState(() {
          isCounting = true;
        });

        _timer = Timer.periodic(const Duration(seconds: 1), (Timer timer) {
          setState(() {
            seconds++;
            if (laps.isNotEmpty) {
              laps[laps.length - 1].second = seconds - lapPoint;
              laps[laps.length - 1].end = seconds;
            }
          });
        });
      },
      style: ButtonStyle(
        minimumSize: MaterialStateProperty.all<Size>(const Size(50, 50)),
        backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
        shape: MaterialStateProperty.all<CircleBorder>(const CircleBorder()),
      ),
    );
  }

  Widget counter() {
    return Center(
      child: SizedBox(
        height: 150,
        width: 300,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  seconds.toString(),
                  style: const TextStyle(fontSize: 30),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ElevatedButton resetButton() {
    return ElevatedButton(
      child: const Text("Reset"),
      onPressed: () {
        // laps.add(Lap());
        // int newLapIndex = laps.length - 1;
        // currentLap = newLapIndex;
        // laps[newLapIndex]._timer =
        setState(() {
          isCounting = false;
          _timer.cancel();
          seconds = 0;
          lapPoint = 0;
          laps = [];
        });
      },
      style: ButtonStyle(
        minimumSize: MaterialStateProperty.all<Size>(const Size(50, 50)),
        backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
        shape: MaterialStateProperty.all<CircleBorder>(const CircleBorder()),
      ),
    );
  }

  ElevatedButton LapButton() {
    return ElevatedButton(
      child: const Text("Lap"),
      onPressed: () {
        // laps.add(Lap());
        // int newLapIndex = laps.length - 1;
        // currentLap = newLapIndex;
        // laps[newLapIndex]._timer =
        setState(() {
          if (laps.length == 0) {
            laps.add(Lap(seconds, seconds));
          }
          lapPoint = seconds;
          laps.add(Lap(0, seconds));
        });
      },
      style: ButtonStyle(
        minimumSize: MaterialStateProperty.all<Size>(const Size(50, 50)),
        backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
        shape: MaterialStateProperty.all<CircleBorder>(const CircleBorder()),
      ),
    );
  }

  Widget lapsView() {
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: laps.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text("Lap " +
                index.toString() +
                ": " +
                (laps[index].second).toString() +
                " - " +
                laps[index].end.toString()),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: counter(),
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment
                    .center, //Center Row contents horizontally,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  if (!isCounting) startButton() else cancelButton(),
                  resetButton(),
                  LapButton(),
                ]),
            Expanded(child: Container(child: lapsView())),
          ]),
    );
  }
}

class Lap {
  int second = 0;
  int end = 0;
  Lap(this.second, this.end);
}
