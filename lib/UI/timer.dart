import 'dart:async';

import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';

class TimerScreen extends StatefulWidget {
  const TimerScreen({Key? key}) : super(key: key);

  @override
  State<TimerScreen> createState() => _TimerScreenState();
}

class _TimerScreenState extends State<TimerScreen> {
  late int seconds;
  bool isCounting = false;
  late Timer _timer;
  int _minsPicked = 0;
  int _hoursPicked = 0;
  late Widget timerPlace;
  late Widget buttonPlace;

  static const TextStyle clockTextStyle =
      TextStyle(fontSize: 50, color: Colors.white);

  String _clockText = "";

  Widget timerCounter() {
    return Stack(
      alignment: Alignment.center,
      children: [
        const Icon(
          Icons.circle,
          size: 300,
          color: Colors.blue,
        ),
        Text(
          _clockText,
          style: clockTextStyle,
        )
      ],
    );
  }

  Widget cancelTimerButton() {
    return ElevatedButton(
      onPressed: () {
        setState(() {
          isCounting = false;
          _timer.cancel();
        });
      },
      child: const Text("Cancel"),
    );
  }

  Widget startTimerButton() {
    return ElevatedButton(
        onPressed: () {
          // print(_hoursPicked.toString() + ": " + _minsPicked.toString());

          setState(() {
            isCounting = true;
            // timerPlace = timerCounter();
            seconds = _hoursPicked * 3600 + _minsPicked * 60;
            _clockText = _hoursPicked.toString() +
                ":" +
                (_minsPicked > 9 ? "" : "0") +
                _minsPicked.toString() +
                ": 00";
          });

          _timer = Timer.periodic(
            const Duration(seconds: 1),
            (Timer timer) {
              if (seconds == 0) {
                setState(() {
                  timer.cancel();
                });
              } else {
                setState(() {
                  seconds--;
                  int mins = ((seconds % 3600) ~/ 60);
                  _clockText = (seconds ~/ 3600).toString() +
                      ":" +
                      (mins > 9 ? "" : "0") +
                      mins.toString() +
                      ":" +
                      ((seconds % 3600) % 60).toString();
                  buttonPlace = cancelTimerButton();
                });
              }
            },
          );
        },
        child: const Text("Start"));
  }

  Widget timerPicker() {
    return Padding(
      padding: const EdgeInsets.all(100.0),
      child: Row(
        mainAxisAlignment:
            MainAxisAlignment.center, //Center Row contents horizontally,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            children: [
              const Text("Hour"),
              NumberPicker(
                value: _hoursPicked,
                minValue: 0,
                maxValue: 1000,
                onChanged: (value) => setState(() => _hoursPicked = value),
              ),
            ],
          ),
          Column(
            children: [
              const Text("Minute"),
              NumberPicker(
                value: _minsPicked,
                minValue: 1,
                maxValue: 60,
                onChanged: (value) => setState(() => _minsPicked = value),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          // Padding(
          //     padding: const EdgeInsets.all(50),
          //     child: Center(
          //         child: Text(
          //       "Timer",
          //       style: clockTextStyle,
          //     ))),
          if (isCounting) timerCounter() else timerPicker(),
          if (isCounting) cancelTimerButton() else startTimerButton()
        ],
      ),
    );
  }
}
